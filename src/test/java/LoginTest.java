import org.testng.annotations.Test;
import pages.BasePage;

import static helpers.testData.*;
import static pages.LoginPage.*;

public class LoginTest extends BasePage {
    @Test(description = "Некорректный логин", priority = 0)
    public void testNoCorrectLogin() {

        //Открываем страницу
        openMainPage();

        //Вводим некорректное значение в поля логина
        enterField(login, nocorrect);
        clickButton(loginClick);
        checkElementVisible(noLoginLabel);


    }

    @Test(description = "Корректный логин", priority = 1)
    public void testCorrect() {

        //Открываем страницу
        openMainPage();

        //водим корректные значения в поля логина и пароля
        enterField(login, correctlogin);
        enterField(password, correctpassword);
        clickButton(loginClick);
        checkElementVisible(correctLabel);
    }

    @Test(description = "Некорректный пароль", priority = 2)
    public void testNoCorrectPassword() {

        //Открываем страницу
        openMainPage();

        //водим корректные значения в поля логина и пароля

        enterField(login, correctlogin);
        enterField(password, nocorrect);
        clickButton(loginClick);
        checkElementVisible(noPasswordLabel);
    }
    }



