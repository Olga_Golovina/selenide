package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import static helpers.testData.*;


import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class LoginPage {

    public static SelenideElement login = $(By.name("username"));
    public static SelenideElement password = $(By.name("password"));
    public static SelenideElement loginClick = $(By.className("radius"));
    public static SelenideElement correctLabel = $(By.xpath("//*[contains(text(),'You logged into a secure area!')]"));
    public static SelenideElement noLoginLabel = $(By.xpath("//*[contains(text(),'Your username is invalid!')]"));
    public static SelenideElement noPasswordLabel = $(By.xpath("//*[contains(text(),'Your password is invalid!')]"));


    //Открыть главную страницу
    public static void openMainPage() {
        open(url);
    }

    //Ожидание пока элемент появится
    public static void waitForElementToAppear(SelenideElement element) {
        element.waitUntil(Condition.appear, 30000);
    }


    //Заполнение поля
    public static void enterField(SelenideElement element, String value) {
        element.waitUntil(Condition.appear, 10000);
        element.setValue("");
        element.setValue(value);
    }


        @Step("Клик на кнопку")
        public static void clickButton(SelenideElement element) {
            JavascriptExecutor executor = (JavascriptExecutor) getWebDriver();
            executor.executeScript("arguments[0].click();", element);

        }



        public static void pressEnterForElement (SelenideElement element){

            element.sendKeys(Keys.ENTER);
        }

    public static SelenideElement checkElementVisible(SelenideElement element) {

        return element.shouldBe(Condition.visible);
    }


    }
