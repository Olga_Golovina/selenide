package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.closeWebDriver;

public class BasePage {
    @BeforeMethod
    public static void beforeMethod() {

        Configuration.browser = myChromeBrowserClass.class.getName();
        Configuration.startMaximized = true;
        Configuration.screenshots = false;

    }

    @AfterMethod
    public void afterMethod() {

        closeWebDriver();
    }

    @Step("Заполнение поля")
    public static void enterField(SelenideElement element, String value) {
        element.waitUntil(Condition.appear, 10000);
        element.setValue("");
        element.setValue(value);
    }




}
